<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Робота з файлми</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div>
		<input class="button" type="button" onclick="history.back(-1)" value="<--Назад"/>
		<br>
		<?php  
		if(isset($_POST['filename'])) $filename = $_POST['filename'];
		if (!file_exists($filename)) {
			if(mkdir($filename))
				echo "Папка $filename создана";
			else
				echo "Ошибка при создании папки";
		} else{
			echo "Папка уже существует!";
		}
		?>	
	</div>	
</body>
</html>