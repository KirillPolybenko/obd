<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Result</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
	<form action="result.php" method="POST">
		<fieldset >
			
			<legend>Тест</legend>
			<p> 
				<img width="500" src="media/c8311b00-4b3b-4012-9e25-6a6f93a73aad.gif"><br>
				<font size="+2"> 1. Кто сделал врата Дурина? <br> </font>	
				<input disabled type="radio" <?php echo ($_POST[q1] == a1) ?  "checked" : "" ;  ?>>Келебримбор 
				<?php $p = 0; if($_POST[q1] == a1){echo '✅ Правильно'; $p++;}?> <br> 
				<input disabled type="radio" <?php echo ($_POST[q1] == a2) ?  "checked" : "" ;  ?>> Элронд
				<?php if($_POST[q1] == a2){echo '❌ Не правильно';}?> <br> 
				<input disabled type="radio" <?php echo ($_POST[q1] == a3) ?  "checked" : "" ;  ?>> Торин Дубощит
				<?php if($_POST[q1] == a3){echo '❌ Не правильно';}?><br>
				
			</p>
			<p> 
				<img width="500" src="media/eb1e48ac-aa71-404b-8beb-0370a5f763aa.gif"> <br>
				<font size="+2"> 2. Сколько лет Кольцо лежало на дне реки, где был убит Исилдур, пока Смеагол не взял его?<br> </font>
				<input disabled type="radio" <?php echo ($_POST[q2] == a1) ?  "checked" : "" ;  ?> >1500 лет 
				<?php if($_POST[q2] == a1){echo '❌ Не правильно';}?> <br> 
				<input disabled type="radio" <?php echo ($_POST[q2] == a2) ?  "checked" : "" ;  ?>>2000 лет
				<?php if($_POST[q2] == a2){echo '❌ Не правильно';}?> <br> 
				<input disabled type="radio" <?php echo ($_POST[q2] == a3) ?  "checked" : "" ;  ?>>2500 лет
				<?php if($_POST[q2] == a3){echo '✅ Правильно'; $p++;}?> <br> 

				
			</p>
			<p>
				<img width="500" src="media/837d4093-0067-430f-81b2-3951c0614d8c.jpg"> <br>
				<font size="+2"> 3. Как связаны между собой Смеагол и Деагол?<br>  </font>
				<input disabled type="radio" <?php echo ($_POST[q3] == a1) ?  "checked" : "" ;  ?>>Друзья
				<?php if($_POST[q3] == a1){echo '❌ Не правильно';}?> <br> 
				<input disabled type="radio" <?php echo ($_POST[q3] == a2) ?  "checked" : "" ;  ?>>Двоюродные братья
				<?php if($_POST[q3] == a2){echo '✅ Правильно'; $p++;}?> <br> 
				<input disabled type="radio" <?php echo ($_POST[q3] == a3) ?  "checked" : "" ;  ?>>Братья
				<?php if($_POST[q3] == a3){echo '❌ Не правильно';}?> <br> 
				
			</p>
			<p>
				<img width="500" src="media/c8ff4aa4-b1c7-4919-af9e-d9e4d7d260b5.gif"> <br>
				<font size="+2"> 4. Какое эльфийское имя у Арагорна?<br> </font>
				<input disabled type="radio" <?php echo ($_POST[q4] == a1) ?  "checked" : "" ;  ?>>Трандуил
				<?php if($_POST[q4] == a1){echo '❌ Не правильно';}?> <br>
				<input disabled type="radio" <?php echo ($_POST[q4] == a2) ?  "checked" : "" ;  ?>>Элессар
				<?php if($_POST[q4] == a2){echo '✅ Правильно'; $p++;}?> <br> 
				<input disabled type="radio" <?php echo ($_POST[q4] == a3) ?  "checked" : "" ;  ?>>Митрандир
				<?php if($_POST[q4] == a3){echo '❌ Не правильно';}?> <br>
			</p>

			
			<?php echo "<p>$_COOKIE[login] сделал ответов: $p</p>" ?>

			<?php
			$fd = fopen("TestResult.txt", 'w') or die("Не удалось создать файл");
			fwrite($fd, "$_COOKIE[login] сделал ответов: $p");
			fclose($fd);
			?>

		</fieldset>
	</fieldset>

</form>
</body>

</html>