<!DOCTYPE html>


<head>
    <meta charset="utf-8">
    <title>Ввод даных</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>


<body>
    <form action="anketa.php" method="post">
        <fieldset>
            <legend>Заполните таблицу</legend>
            <p>
                <label>Логин:</label>
                <input type="text" name="login" placeholder="Введите логин" />
            </p>
            <p>
                <label for="email">Email:</label>
                <input required type="email" placeholder="user@gmail.com" name="email" />
            </p>
            <p>
                <label>Имя:</label>
                <input required type="text" name="userName" placeholder="Введите имя" />
            </p>
            <p>
                <label>Пароль:</label>
                <input required type="password" name="password" placeholder="Введите пароль" />
            </p>

            <p>
                <label>Дата рождения:</label>
                <input type="date" name="date" />
            </p>
            <p>
                <label for="tel">Номер телефона:</label>
                <input required type="tel" name="phoneNumber" pattern="\+\d{2}\d{3}\d{3}\d{4}" placeholder="+380XXXXXXXXX" />
            </p>


            <p>
                <label>Выберите язык для изучения:</label>
                <br>
                <input type="checkbox" value=".NET" name="dotnet" />.NET
                <br>
                <input type="checkbox" value="Java" name="java" />Java
            </p>

            <p> <b>Пол:</b>
                <input type="radio" value="Мужчина" name="gender" />Мужской.
                <input type="radio" value="Женщина" name="gender" />Женский.
            </p>
            <p> <b>Cемейный статус:</b>
                <input type="radio" value="jenat"  name="semsta" />Женат/Замужем.
                <input type="radio" value="njenat" name="semsta" />Не женат/замужем.
            </p>
            <p>
                1. У меня отличное чувство юмора
                <br>
                <input type="radio" value="yes" name="1test" />Да
                <br>
                <input type="radio" value="probably yes" name="1test" />Скорее «Да», чем «Нет»
                <br>
                <input type="radio" value="probably no" name="1test" />Скорее «Нет», чем «Да»
                <br>
                <input type="radio" value="no" name="1test" />Нет
                <br>
            </p>
            <p>
                2. Моя интуиция никогда не ошибается
                <br>
                <input type="radio" value="yes" name="2test" />Да
                <br>
                <input type="radio" value="probably yes" name="2test" />Скорее «Да», чем «Нет»
                <br>
                <input type="radio" value="probably no" name="2test" />Скорее «Нет», чем «Да»
                <br>
                <input type="radio" value="no" name="2test" />Нет
                <br>
            </p>
            <p>
                3. Мое будущее кажется мне благоприятным и позитивным
                <br>
                <input type="radio" value="yes" name="3test" />Да
                <br>
                <input type="radio" value="probably yes" name="3test" />Скорее «Да», чем «Нет»
                <br>
                <input type="radio" value="probably no" name="3test" />Скорее «Нет», чем «Да»
                <br>
                <input type="radio" value="no" name="3test" />Нет
                <br>
            </p>
            <p>4. Вопрос
                <br>
                <input type="checkbox" name="answer4.1" />Ответ 4.1
                <br>
                <input type="checkbox" name="answer4.2" />Ответ 4.2
                <br>
                <input type="checkbox" name="answer4.3" />Ответ 4.3
                <br>
                <input type="checkbox" name="answer4.3" />Ответ 4.4
                <br>
            </p>
            <p>5. Вопрос
                <br>
                <input type="checkbox" name="answer5.1" />Ответ 5.1
                <br>
                <input type="checkbox" name="answer5.2" />Ответ 5.2
                <br>
                <input type="checkbox" name="answer5.3" />Ответ 5.3
                <br>
                <input type="checkbox" name="answer5.3" />Ответ 5.4
                <br>
            </p>

            <p>
                <label for="comment">О себе:</label>
                <br/>
                <textarea name="comment" placeholder="Не более 200 символов" maxlength="200"></textarea>
            </p>

            <p><button name="send" type="submit">Сохранить</button></p> 
        </fieldset>
    </form>
</body>
