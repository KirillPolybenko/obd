<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Тест</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
	<form action="result.php" method="POST">
		<fieldset>	
			<legend>Тест</legend>
			<?php if($_COOKIE['user'] != ''): ?>
				
				<?php 
				$user = $_COOKIE['user'];
				echo "Здравствуйте $user<br><a href='exit.php'>выйти</a>";
				?>
			<?php endif; ?>
			<p>
				<img width="500" src="media/c8311b00-4b3b-4012-9e25-6a6f93a73aad.gif"><br>
				<font size="+2"> 1. Кто сделал врата Дурина? <br> </font>
				<input type="radio" value="a1" name="q1">Келебримбор<br> 
				<input type="radio" value="a2" name="q1">Элронд<br>
				<input type="radio" value="a3" name="q1">Торин Дубощит<br>
				
			</p>
			<p>
				<img width="500" src="media/eb1e48ac-aa71-404b-8beb-0370a5f763aa.gif"> <br>
				<font size="+2"> 2. Сколько лет Кольцо лежало на дне реки, где был убит Исилдур, пока Смеагол не взял его?<br> </font>
				<input type="radio" value="a1" name="q2">1500 лет<br> 
				<input type="radio" value="a2" name="q2">2000 лет<br>
				<input type="radio" value="a3" name="q2">2500 лет<br>
				
			</p>
			<p>
				<img width="500" src="media/837d4093-0067-430f-81b2-3951c0614d8c.jpg"> <br>
				<font size="+2"> 3. Как связаны между собой Смеагол и Деагол?<br>  </font>
				<input type="radio" value="a1" name="q3">Друзья<br> 
				<input type="radio" value="a2" name="q3">Двоюродные братья<br>
				<input type="radio" value="a3" name="q3">Братья<br>
				
			</p>
			<p>
				<img width="500" src="media/c8ff4aa4-b1c7-4919-af9e-d9e4d7d260b5.gif"> <br>
				<font size="+2"> 4. Какое эльфийское имя у Арагорна?<br> </font>
				<input type="radio" value="a1" name="q4">Трандуил<br>
				<input type="radio" value="a2" name="q4">Элессар<br> 
				<input type="radio" value="a3" name="q4">Митрандир<br>
				
			</p>

			<button class="center" type="submit">Отправить</button>

		</fieldset>


	</form>
</body>

</html>