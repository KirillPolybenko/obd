<!DOCTYPE html>


<head>
    <meta charset="utf-8">
    <title>Ввод даных</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>


<body>
    <form action="check.php" method="post">
        <fieldset>
            <legend>Заполните таблицу</legend>
            <p>
                <label>Логин:</label>
                <input type="text" name="login" placeholder="Введите логин" />
            </p>
            <p>
                <label for="email">Email:</label>
                <input required type="email" placeholder="user@gmail.com" name="email" />
            </p>
            <p>
                <label>Имя:</label>
                <input required type="text" name="name" placeholder="Введите имя" />
            </p>
            <p>
                <label>Пароль:</label>
                <input required type="password" name="pass" placeholder="Введите пароль" />
            </p>


            <p>
                <label for="tel">Номер телефона:</label>
                <input required type="tel" name="phoneNumber" pattern="\d{2}\d{3}\d{3}\d{4}" placeholder="380XXXXXXXXX" />
            </p>

            <p>
                <label>Выберите язык для изучения:</label>
                <br>
                <input type="checkbox" value=".NET" name="dotnet" />.NET
                <br>
                <input type="checkbox" value="Java" name="java" />Java
            </p>

            <p> <b>Пол:</b>
                <input type="radio" value="Мужчина" name="gender" />Мужской.
                <input type="radio" value="Женщина" name="gender" />Женский.
            </p>
            <p> <b>Cемейный статус:</b>
                <input type="radio" value="jenat"  name="semsta" />Женат/Замужем.
                <input type="radio" value="njenat" name="semsta" />Не женат/замужем.
            </p>
            <p><button name="send" type="submit">Сохранить</button></p> 
        </fieldset>
    </form>
</body>
